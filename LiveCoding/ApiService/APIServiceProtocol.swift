//
//  APIServiceProtocol.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation

typealias ApiResponseClosure = (Data?,URLResponse?,Error?)->Void

protocol ProviderType {
  var baseURL:URL {get}
  var path:String? {get}
  var header:[String:String]? {get}
  var method:HTTPMethod {get}
}

protocol APIServiceRequestAble {
  associatedtype Provider = ProviderType
  func request(_ provider: Provider, completion: @escaping ApiResponseClosure)->URLSessionDataTask
}
