//
//  APIServiceDecoder.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation

protocol DecodeAble {
  associatedtype ObjectType
  func decode(_ data:Data?) -> ObjectType?
}

class JsonDecoder<T:Decodable>:DecodeAble {
  typealias ObjectType = T
  
  func decode(_ data: Data?) -> ObjectType? {
    let decoder = JSONDecoder()
    guard let data = data, let model = try? decoder.decode(T.self, from: data) else {return nil}
    return model
  }
}
