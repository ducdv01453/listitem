//
//  APIService.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation

class APIService<Provider:ProviderType>: APIServiceRequestAble {
  @discardableResult
  func request(_ provider: Provider, completion: @escaping ApiResponseClosure)->URLSessionDataTask {
    
    //
    let request = URLRequest(url: URL(target: provider))
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      completion(data,response,error)
    }
    task.resume()
    return task
  }
}
