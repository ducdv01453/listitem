//
//  APIServiceHelper.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation

enum HTTPMethod:String {
  case get = "GET"
  case post = "POST"
}
