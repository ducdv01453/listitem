//
//  URLExtension.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation
extension URL {
  init<T: ProviderType>(target: T) {
    if let targetPath = target.path  {
      if targetPath.isEmpty {
        self = target.baseURL
      }else {
        self = target.baseURL.appendingPathComponent(targetPath)
      }
    }else {
      self = target.baseURL
    }
  }
}
