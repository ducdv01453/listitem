//
//  ItemTableViewCell.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
  @IBOutlet weak var lbName: UILabel!
  @IBOutlet weak var lbPrice: UILabel!
  @IBOutlet weak var lbId: UILabel!

  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configCellWithModel(_ model:APIModel) {
    self.lbName.text = model.name
    self.lbPrice.text = "\(model.price)"
    self.lbId.text = model.id
  }
}
