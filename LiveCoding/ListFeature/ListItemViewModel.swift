//
//  ListFeatureViewModel.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation

enum DataState {
  case initial
  case loadmore
}

class ListItemViewModel {
  private let paggingOffSet:Int = 2

  private var listItem:[APIModel] = []

  var dataChangeWithState : ((DataState) -> Void)?

  
  var numberOfItems: Int {
    return self.listItem.count
  }
  
  // MARK: - Public
  func itemForIndex(_ index:Int) -> APIModel? {
    if index < 0 && index >= listItem.count {
        return nil
    }
    return listItem[index]
  }
  
  func fetchDataFromRemoteForState(_ dataState:DataState) {
    let service = APIService<AppProvider>()
    service.request(.getList) { [weak self] data, response, error in
      guard let self = self else {return}
      if let decoder = JsonDecoder<[APIModel]>().decode(data) {
        switch dataState {
        case .initial:
          self.listItem = decoder
        case .loadmore:
          self.listItem.append(contentsOf: decoder)
        }
        if let dataChangeWithState = self.dataChangeWithState {
          dataChangeWithState(dataState)
        }
      }
    }
  }
  
  func pagingOffset() -> Int {
    return listItem.count - paggingOffSet
  }
}
