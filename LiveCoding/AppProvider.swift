//
//  AppProvider.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation

enum AppProvider {
  case getList
  case otherApi
}

extension AppProvider : ProviderType {
  var header: [String : String]? {
    switch self {
    case .getList:
      return ["Content-type": "application/json"]
    default:
      return nil
    }
  }
  
  var method: HTTPMethod {
    switch self {
    case .getList:
      return .get
    default:
    return .get
    }
  }
  
  var baseURL: URL {
    return URL(string: "https://61c38d139cfb8f0017a3ec16.mockapi.io")!
  }
  
  var path: String? {
    switch self {
    case .getList:
      return "/api/v1/items"
    default:
      return nil
    }
  }
  
}
