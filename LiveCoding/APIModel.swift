//
//  APIModel.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import Foundation
struct APIModel: Codable {
  var name:String
  var id:String
  var price:Int
}
