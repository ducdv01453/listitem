//
//  ViewController.swift
//  LiveCoding
//
//  Created by duc do viet on 23/05/2022.
//

import UIKit

class ViewController: UIViewController {
  let viewModel = ListItemViewModel()

  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    configureTableView()
    initalizeData()
  }
  
  private func initalizeData(){
    viewModel.fetchDataFromRemoteForState(.initial)
    observerDataChange()
  }
  
  private func configureTableView(){
    tableView.dataSource = self
    tableView.delegate = self
    registerCell()
  }
  
  private func registerCell () {
    let nib = UINib(nibName: "ItemTableViewCell", bundle: Bundle.main)
    tableView.register(nib, forCellReuseIdentifier: "ItemTableViewCell")
  }

  private func observerDataChange() {
    viewModel.dataChangeWithState = { (dataState) in
      DispatchQueue.main.async {
        switch dataState {
        case .initial:
          self.tableView.reloadData()
          break
        case .loadmore:
          // Need to do table update but currently we use table reload for temporary solution
          self.tableView.reloadData()
          break
        }
      }
    }
  }
  
  
  
}

// MARK: UITableViewDataSource
extension ViewController:UITableViewDataSource,UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfItems
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
    guard let model = viewModel.itemForIndex(indexPath.row) else {return UITableViewCell()}
    cell.configCellWithModel(model)
    return cell
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if (indexPath.row == viewModel.pagingOffset()) {
      viewModel.fetchDataFromRemoteForState(.loadmore)
    }
  }
}
